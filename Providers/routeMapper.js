const register = require('../Service/user.service').register;
const login = require('../Service/user.service').login;
const addMovie =  require('../Service/admin/adminMovie.service').addMovie;
const postRating =  require('../Service/movie.service').postRating;
const movieDetails = require('../Service/movie.service').getMovieDetails;
module.exports = {
    register,
    login,
    addMovie,
    postRating,
    movieDetails
}
