const mongoose = require('mongoose');
var globals = require('../Providers/globals')();

const connectDB = async () => {
    return await mongoose.connect(`${globals.MONGO_URI}/${globals.DB_NAME}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }).then(conn => {
        console.log(`MongoDB Connected: ${conn.connection.host}`);
    }).catch(err => {
        console.error(err);
    });
}

module.exports = connectDB;