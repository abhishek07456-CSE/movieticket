module.exports  = function () {
    const port = process.env.PORT || 9090;
    const url = process.env.APP_URL || `http://127.0.0.1:${port}`;
    const MONGO_URI  = process.env.MONGO_URI || `mongodb://localhost:27017`;
    const DB_NAME = "movie_ticket";
    const TOKEN_SECRET = "26c601c19cbfc02d03b80887026a3c6f4b28d828c4ccdd4cad9bc74b21bebf77";
    const TOKEN_EXPIRE_TIME = process.env.TOKEN_EXPIRE_TIME || "1800s";
    return {
        port,
        url,
        MONGO_URI,
        DB_NAME,
        TOKEN_SECRET,
        TOKEN_EXPIRE_TIME
    }
}