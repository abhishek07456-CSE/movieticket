const gender = {
    '' : '',
    'm' : 'Male',
    'f' : 'Female',
    'o' : 'Other'
}

const SupportingRole = {
     'pd' : "Producer",
     "ma" : "Male actor",
     "fa" : "Female actor",
     "mc" : "Music Compose",
     "wr" : "Writer",
     "ms" : "Male Singer",
     "fs" : "Female Singer"
}

// add global list
module.exports = {
    gender,
    SupportingRole
};