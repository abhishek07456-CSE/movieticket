var express = require('express');
var router = express.Router();
var routeMapper = require('../Providers/routeMapper');
var Auth = require('../Middleware/Auth');
 
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


router.post('/register', routeMapper.register);
router.post('/login', Auth.refreshToken  , routeMapper.login);

module.exports = router;
