var express = require('express');
var router = express.Router();
var routeMapper = require('../Providers/routeMapper');
var Auth = require('../Middleware/Auth');

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/add', Auth.authenticateJWT , routeMapper.addMovie);//added by
router.post('/postRating', Auth.authenticateJWT, routeMapper.postRating);
router.post('/movieDetails' , routeMapper.movieDetails)
module.exports = router;
