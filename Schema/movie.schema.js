const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Auth = require('../Middleware/Auth');
ObjectId = Schema.ObjectId;
const list = require("../Providers/globalList");
const movieSchema = new mongoose.Schema({
    movieName: {
        type: String,
        required: true,
        unique:true,
        lowercase:true,
        trim:true
    },
    description: {
        type: String,
        required: true
    },
    // supportingPeople: [{ObjectId , ref: supportingPeople}],
    // audienceResponse: [{ObjectId , ref: audienceResponse.audienceResponseSchema}],
    //language support
    language: {
        type: [String],
        validate: v => Array.isArray(v) && v.length > 0,
    },
    addedBy: {
        type : ObjectId,
        ref:"users"
    }
  },
  {
        timestamps: true
  }
);
movieSchema.pre('save' , function (next) {
    if (this.isNew) {
        this.addedBy = Auth.loggedUser;
    }
    next();
});
movieSchema.statics = {
	findByMovieName: function(name, callback){
		return this
			.findOne({movieName : name})
			.exec(callback)
	}
};
const movieModel = mongoose.model('movie', movieSchema);
module.exports = movieModel;
