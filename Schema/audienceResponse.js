const mongoose = require('mongoose');
const Auth = require('../Middleware/Auth');
const Schema = mongoose.Schema;
ObjectId = Schema.ObjectId;

const audienceResponseSchema = new mongoose.Schema({
    comment: {
        type: String,
    },
    rating: {
        type: Number,
        min: 1,
        max: 10,
        required:true
    },
    movie: {
        type: ObjectId,
        required:true
    },
    givenBy: {
        type: ObjectId,
        ref:"users"
    }
  },
  {
        timestamps: true
    }
);
audienceResponseSchema.statics = {
	findByMovieId: function(movie, callback){
        console.log(movie);
		return this
			.find({movie : movie.id})
            .sort({createdAt: -1, rating: -1}).
            limit(movie.limit).
            skip(movie.skip).
			exec(callback)
	},
    findOverallRating: function(movie_id , callback){
        return this.aggregate([
            {$match: {movie : movie_id}},
            {
                $group:
                {
                    _id: "$movie",
                    totalRating: { $sum: 1 },
                    averageRating: { $avg: "$rating" }
                }
            }
        ]).exec(callback);
   }
};
audienceResponseSchema.pre('save' , function(next){
    if (this.isNew) {
        this.givenBy = Auth.loggedUser;
    }
    next();
});
const audienceResponseModel = mongoose.model('audienceResponse', audienceResponseSchema);
module.exports = audienceResponseModel;
