const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const list = require("../Providers/globalList");
ObjectId = Schema.ObjectId;
const supportingPeopleSchema = new mongoose.Schema({
    role: {
        type: String,
        // get : function(value){
        //     return list.SupportingRole[value];
        // }
        // validate : value => Object.values(list.SupportingRole).includes(value),
    },
    name: {
        type: String,
    },
    movie : {
        type : ObjectId
    }
});
supportingPeopleSchema.set('toJSON', { virtuals: true });
supportingPeopleSchema.virtual('role_description').get(function () {
    return list.SupportingRole[this.role];
});
const supportingPeopleModel = mongoose.model('supportingPeople', supportingPeopleSchema);
module.exports = supportingPeopleModel;