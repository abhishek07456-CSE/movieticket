const mongoose = require("mongoose");
const validator= require('validator');
const Crypto = require("../Providers/Crypto");
const list = require("../Providers/globalList");
const schema = mongoose.Schema;
let definition = new schema (
    {
        email: {
            type: schema.Types.String,
            required: true,
            trim: true,
            lowercase: true,
            unique: true,
            validate: (value) => {
                return validator.isEmail(value)
            }
        },
        firstName: {
            type: schema.Types.String,
            required: true,
            trim: true,
        },
        lastName: {
            type: schema.Types.String,
            required: true,
            trim: true,
        },
        password: {
            type: schema.Types.String,
            required: true,
            validate: (value) => {
                return validator.isStrongPassword(value)
            },
            set: (value) => {
                return Crypto.encrypt(value);
            }
        },
        gender: {
            type: schema.Types.String,
            enum: Object.keys(list.gender),//from predefined list
            default : ''
        },
        age: {
            type: schema.Types.Number,
            min: 0,
            default: 0
        },
    },
    {
        timestamps: true
    }
);
definition.statics = {
	findByEmail: function(email, callback){
		return this
			.findOne({email: email})
			.exec(callback)
	}
};
const UserSchema = mongoose.model("user", definition);

module.exports = UserSchema;