const Crypto = require("../Providers/Crypto");
const userService = require("../Service/user.service");
const globals = require('../Providers/globals')();
const userModel = require('../Schema/user.schema');
const jwt = require('jsonwebtoken');

module.exports = class Auth {
     static loggedUser = null;
     static authenticateJWT = (req, res, next) => {
          const authHeader = req.headers.authorization;
          if (authHeader) {
               const token = authHeader.split(' ')[1];

               jwt.verify(token, globals.TOKEN_SECRET, async (err, user) => {
                    if (err) {
                         return res.status(401).send(err);
                    }
                    Auth.loggedUser = user.id;
                    next();
               });
          } else {
               res.status(401).send({ error: "Token Not Found" });
               next();
          }
     };
     static refreshToken = async (request, response, next) => {
          const user = await userModel.findByEmail(request.body.email);
          if (user == null || user._id == undefined) {
               return response.status(401).json({ "msg": "User Not Found" });
          } else if (!Crypto.compareHash(request.body.password, user.password)) {
               return response.status(401).json({ "msg": "Password Incorrect" });
          }
          const token = jwt.sign({ id: user._id, email: user.email }, globals.TOKEN_SECRET, { expiresIn: globals.TOKEN_EXPIRE_TIME });
          Auth.loggedUser = user._id;
          response.data = { ...user._doc };
          response.data.token = token;
          console.log(response.data);
          next();
     }
}