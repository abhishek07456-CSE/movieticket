var userModel = require('../Schema/user.schema');
const register = (req, res, next) => {
   var user = new userModel(req.body)
   user.save((err, user) => {
      if (err)
      return res.status(400).send({
         message: (err?.keyPattern?.email === 1 && err.code === 11000) ? 'Email already exists !' : err
      });
      res.status(200).json(user);
      next();
   });
}
const login = async (req,res,next) => {
    res.status(200).json(res.data);
    next();
}
module.exports = {
   register,
   login
}