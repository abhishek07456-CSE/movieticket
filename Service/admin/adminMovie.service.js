var movieModel = require('../../Schema/movie.schema');
var supportingPeopleModel = require('../../Schema/supportingPeople');

const list = require("../../Providers/globalList");
const addMovie = (req, res, next) => {
   var movieData = JSON.parse(JSON.stringify(req.body));
   var supportingRole = movieData.supportingRole || null;
   if(supportingRole){
      Object.keys(supportingRole).forEach(value => {
         if(!Object.keys(list.SupportingRole).includes(value))
          res.status(401).json({"message" : `Invalid Supporting Role Id ${value}`});
      })
   }
   delete movieData?.supportingRole;
  
   const movie = new movieModel(movieData);

   movie.save(async (err, movie) => {
      if (err){
            return res.status(400).send({
               message: (err?.keyPattern?.movieName === 1 && err.code === 11000) ? 'Movie already exists!' : err
            });
      }
      const MovieAllData = {...movie._doc};
      let rolePeople = [];
      for(const [key, value]  of Object.entries(supportingRole)){
            const supportingPeople = new supportingPeopleModel({role : key , value : value , movie : movie._id});
              let data = await supportingPeople.save();
                  data = {...data._doc}
              data.role = list.SupportingRole[key];
              rolePeople.push(data);
      }
      MovieAllData['supportingPeople'] = rolePeople;
      delete rolePeople;
      res.status(200).json(MovieAllData);
      next();
   });
}
module.exports = {
    addMovie
 }

