var audienceResponseModel = require('../Schema/audienceResponse');
var movieModel = require('../Schema/movie.schema');
var supportingPeopleModel = require('../Schema/supportingPeople');
const postRating = (req, res, next) => {
   const responseModel = new audienceResponseModel(req.body);
   responseModel.save(async (err, response) => {
      if (err) {
          res.status(400).send(err);
      }
      res.status(200).json({ "message": "Response Added Successfully" });
      next();
   });
}

const supportingPeople = (movie_id) => {
   return supportingPeopleModel.find({ movie: movie_id });
}
const getMovieDetails = (req, res, next) => {
   const page = req.body.page_no || 1;
   const items = req.body.no_of_items || 10;
   const skip = (page - 1) * items;
   const promise = new Promise((resolve, reject) => {
      movieModel.findByMovieName(req.body.movieName, (err, movie) => {
         if (err) reject(err);
         let result = { ...movie._doc };
         supportingPeopleModel.find({ movie: movie._id }, (err, people) => {
            if (err) reject(err);
            result.supportingPeople = people;
            audienceResponseModel.findByMovieId({id : movie._id , skip : skip , limit: items}, (err, audienceResponses) => {
               if (err) reject(err);
               result.audienceResponse = audienceResponses;
               audienceResponseModel.findOverallRating(movie._id, (err, rating) => {
                  if (err) reject(err);
                  result.overallRating = rating;
                  resolve(result);
               });
            });
         });
      });
   }).then(data => {
      res.status(200).json(data);
   }).catch(error => {
      res.status(401).json(error);
   });
}

module.exports = {
   postRating,
   getMovieDetails
}

